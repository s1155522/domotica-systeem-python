# README #

Deze readme is opgesteld zodat het ontwikkelen straks makkelijk verloopt.
Hou de format hieronder bij om conflikten te voorkomen.

### Starten in de ontwikkel omgeving ###

Met de start van de ontwikkelomgeving moet je het volgende doen.
Eerst ga je de git flow instellen voor jezelf. 
Git flow is een extentie van git die ervoor zorgt dat er geen conflicten voorkomen tijdens het ontwikkelen.

De git flow omgeving zet je op door in de project folder het volgende commando uit te voeren:

`` git flow init ``

Als je dit commando uitgevoerd hebt krijg je een aantal keuzes.
Hier moet je niks invullen gewoon een aantal keer op enter drukken.
Door dit te hebben gedaan is alle configuratien in onze git omgeving hetzelfde.

Nadat je dit hebt gedaan kom je als het goed is in de develop branch terecht.
Dit is de branch waar alle features uit starten.
Als er een nieuwe feature wordt gemaakt wordt de code uit deze branch gecopieerd naar jouwn gemaakte feature.


### Start van een nieuwe feature ###

Om een nieuwe feature te starten begin je als volgt.
Als eerst kijk je of je in de branch develop zit. dit kan je checken door het volgende te doen.
In je project folder voer je het volgende commando uit.

`` git branch ``

Als je deze commando uitgevoerd hebt zie je de volgende output.


 * develop
   main

De branch met het sterretje ervoor is de branch waar je op dat moment in zit.
Als het steretje niet naast het woord develop staat voer je het volgende commando uit.

`` git checkout develop ``

Dit zorgt ervoor dat je naar de branch develop toe gaat.
Vanuit hier kan je een nieuwe feature starten.

Een nieuwe feater start je door het volgende commando

`` git flow feature start {naam van de feature hier deze mag geen spaties bevatten. Houd de naam van d kaartjes die in de trello staan. } ``

Na het uitvoeren van dit commando zijn er een aantal dingen gebeurt.

* Er is een nieuwe branch aangemaakt.
* De op dat momente data die in develop staat is meegenomen naar die branch
* Je werk omgeving wordt gelijk naar die branch toe gezet.

nu kan je beginnen met ontwikkelen.

### aan het eind van je ontwikkeldag ###

Als je een bepaalde dag bent wezen ontwikkelen en je stopt voor die dag met ontwikkelen dan push je jouw data naar de bitbucket servers toe.
Dit doe je door de volgende commando's uit te voeren.

`` git add {de bestanden waar jij mee bezig bent geweest. als je met meerdere bestanden bezig bent geweest doe je het volgende {bestandnaam.1, bestandnaam.2 ect} voor de bestanden waar jij in bezig  bent geweest de haakjes moeten er uiteraard niet omheen.} ``

Dit commando zorgt ervoor dat jouw weizigingen naar jouw locale tijdlijn worden gezet.
Vervolgens geef je de weizingen een naam dit doe je door hetvolgende commando uit de voeren.

``` git commit -m "{met de naam van de weiziging. gebruik hiervoor geen generieken naamen zoals weiziging1 of iets in die richting beschrijf duidelijk wat de weiziging inhoud}" ```

Als dat gedaan is kan je de data naar de server toe sturen.
dit doe je met het volgende commando.

``` git push ```

Het kan zijn dat je de branch nog naar de server toe moet pushen.
Dan komt je commando er alsvolgt uit te zien.

``` git push --set-upstream origin feature/{naam van de branch} ```

Als dit zo is geeft de terminal dat zelf aan.
Als dat gedaan is ben je klaar voor die dag en kan je je ide afsluiten.


### als je klaar bent met het ontikkelen van de feature ###

Ben je klaar met het ontwikkelen van de feature.
Dan is het volgende van belang.

* check of echt alles van de feature overeenkomt met het fo.
* check met de debugger of er geen fouten tussen zitten.
* check of er geen System.out.print(ln) meer tussen de code staat.

als dit allemaal goed is kan je de feature afsluiten.
dit doe je als volgt.

Eerst zet je al jouw weizigingen op de server.
Dit doe je door de volgende commando's uit te voeren.

`` git add {de bestanden waar jij mee bezig bent geweest. als je met meerdere bestanden bezig bent geweest doe je het volgende {bestandnaam.1, bestandnaam.2 ect} voor de bestanden waar jij in bezig  bent geweest de haakjes moeten er uiteraard niet omheen.} ``

Dit commando zorgt ervoor dat jouw weizigingen naar jouw locale tijdlijn worden gezet.
Vervolgens geef je de weizingen een naam dit doe je door hetvolgende commando uit de voeren.

``` git commit -m "{met de naam van de weiziging. gebruik hiervoor geen generieken naamen zoals weiziging1 of iets in die richting beschrijf duidelijk wat de weiziging inhoud}" ```

Als dat gedaan is kan je de data naar de server toe sturen.
dit doe je met het volgende commando.

``` git push ```

Het kan zijn dat je de branch nog naar de server toe moet pushen.
Dan komt je commando er alsvolgt uit te zien.

``` git push --set-upstream origin feature/{naam van de branch} ```

Als dat gedaan is en de data staat op de server dan kan je de feature sluiten.
Dit doe je door hetvolgende commando uit te voeren.

``` git flow feature finish {met de naam van de feature deze kan je vinden door git branch te doen en dan staat er bij het sterretje iets als feature/[naam van de feature] deze kan hier ingevoerd worden.}  ```

Als dat gedaan is kan je de volgende feature weer starten en beginnen met een nieuwe feature.

